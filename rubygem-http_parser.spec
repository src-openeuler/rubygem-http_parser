%global _empty_manifest_terminate_build 0
%global gem_name http_parser.rb
Name:		rubygem-http_parser
Version:	0.8.0
Release:	1
Summary:	Simple callback-based HTTP request/response parser
License:	MIT
URL:		https://github.com/tmm1/http_parser.rb
Source0:	https://rubygems.org/gems/http_parser.rb-%{version}.gem

BuildRequires:	ruby
BuildRequires:	ruby-devel
BuildRequires:	rubygems
BuildRequires:	rubygems-devel
BuildRequires:	gcc
BuildRequires:	gdb rsync
Provides:	rubygem-http_parser.rb

%description
Ruby bindings to https://github.com/joyent/http-parser and https://github.com/http-parser/http-parser.java

%package help
Summary:	Development documents and examples for http_parser.rb
Provides:	rubygem-http_parser.rb-doc
BuildArch: noarch

%description help
Ruby bindings to https://github.com/joyent/http-parser and https://github.com/http-parser/http-parser.java

%prep
%autosetup -n http_parser.rb-%{version}
gem spec %{SOURCE0} -l --ruby > http_parser.rb.gemspec

%build
gem build http_parser.rb.gemspec
%gem_install

%install
mkdir -p %{buildroot}%{gem_dir}
cp -a .%{gem_dir}/* %{buildroot}%{gem_dir}/
rsync -a --exclude=".*" .%{gem_dir}/* %{buildroot}%{gem_dir}/
if [ -d .%{_bindir} ]; then
	mkdir -p %{buildroot}%{_bindir}
	cp -a .%{_bindir}/* %{buildroot}%{_bindir}/
fi
if [ -d ext ]; then
	mkdir -p %{buildroot}%{gem_extdir_mri}/
	if [ -d .%{gem_extdir_mri}/%{gem_name} ]; then
		cp -a .%{gem_extdir_mri}/%{gem_name}/*.so %{buildroot}%{gem_extdir_mri}/
	else
		cp -a .%{gem_extdir_mri}/*.so %{buildroot}%{gem_extdir_mri}/
fi
	cp -a .%{gem_extdir_mri}/gem.build_complete %{buildroot}%{gem_extdir_mri}/
	rm -rf %{buildroot}%{gem_instdir}/ext/
fi
rm -rf %{buildroot}%{gem_instdir}/{.github/*,.gitignore,.gitmodules}
pushd %{buildroot}
touch filelist.lst
if [ -d %{buildroot}%{_bindir} ]; then
	find .%{_bindir} -type f -printf "/%h/%f\n" >> filelist.lst
fi
popd
mv %{buildroot}/filelist.lst .

%files -n rubygem-http_parser -f filelist.lst
%dir %{gem_instdir}
%{gem_instdir}/*
%{gem_extdir_mri}
%exclude %{gem_cache}
%{gem_spec}

%files help
%{gem_docdir}/*

%changelog
* Fri Jul 14 2023 liyanan <thistleslyn@163.com> - 0.8.0-1
- Update to 0.8.0

* Wed Aug 25 2021 wutao <wutao61@huawei.com> - 0.7.0-2
- Move .so file from /usr/lib64/gems/ruby/http_parser.rb-0.7.0/http_parser.rb/ to
  /usr/lib64/gems/ruby/http_parser.rb-0.7.0/ to solve fluentd loaderror problem

* Mon Aug 02 2021 Ruby_Bot <Ruby_Bot@openeuler.org> - 0.7.0-1
- Package Spec generated
